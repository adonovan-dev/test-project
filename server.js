const express = require("express");
const app = express();
const port = process.env.PORT || 5000;
const dbConfig = require("./config/db");
const bodyParser = require("body-parser");

app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

const MongoClient = require("mongodb").MongoClient;
const ObjectID = require("mongodb").ObjectID;
MongoClient.connect(dbConfig.connectionString, { useUnifiedTopology: true })
  .then((client) => {
    const db = client.db("project");

    app.listen(port, () => console.log(`Listening on port ${port}`));

    app.get("/list", (req, res) => {
      db.collection("expenses")
        .find()
        .toArray()
        .then((results) => {
          res.send({ expenses: results });
        })
        .catch((error) => console.error(error));
    });

    app.post("/create", (req, res) => {
      db.collection("expenses")
        .insertOne(req.body.expense)
        .then((result) => {
          res.redirect("/");
        })
        .catch((error) => console.error(error));
    });
    app.put("/edit/:expenseId", (req, res) => {
      db.collection("expenses")
        .updateOne(
          { _id: ObjectID(req.params.expenseId) },
          { $set: { ...req.body.expense } }
        )
        .then((results) => {
          res.send(results);
        })
        .catch((error) => console.error(error));
    });
    app.delete("/delete/:expenseId", (req, res) => {
      console.log(req.body);
      db.collection("expenses")
        .remove({ _id: ObjectID(req.params.expenseId) })
        .then((results) => {
          res.send(results);
        })
        .catch((error) => console.error(error));
    });
  })
  .catch((error) => console.error(error));
