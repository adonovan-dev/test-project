import React, { useEffect, useReducer } from "react";

import ExpenseModal from "./Modals/ExpenseModal";

const CreateFormModal = ({ showModal, closeModal }) => {
  // Closes expense creation modal
  const closeCreateModal = () => {
    this.setState({ showCreateModal: false });
  };

  // Sends POST request to create expense
  const handleCreate = (data) => {
    fetch("/create", {
      method: "POST",
      body: JSON.stringify({
        expense: data,
      }),
      headers: { "Content-Type": "application/json" },
    }).then((response) => {
      if (response.status === 200) {
        alert("Save Success");
        // Dirty hack since Redux store is not implemented
        window.location.reload();
      } else {
        alert("Error");
      }
      closeModal();
    });
  };

  return (
    <ExpenseModal
      showModal={showModal}
      handleClose={closeModal}
      onSave={handleCreate}
    />
  );
};

export default CreateFormModal;
