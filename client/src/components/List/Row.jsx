import React, { useReducer } from "react";
import Button from "react-bootstrap/Button";

const Row = ({ expense, handleSave, handleDelete }) => {
  const initialState = { ...expense, editMode: false };

  const reducer = (state, { field, value }) => {
    switch (field) {
      case "amount":
        // Set tax value on amount change
        return {
          ...state,
          taxes: (Math.ceil(value * 0.15 * 20) / 20).toFixed(2),
          [field]: value.replace(/^0+/, ""),
        };
      case "START_EDIT":
        return { ...state, editMode: true };
      case "CANCEL":
        return { ...initialState };
      case "SAVE":
        const { _id, description, amount, taxes, date } = state;
        handleSave({ _id, description, amount, taxes, date });
        return { ...state, editMode: false };
      case "DELETE":
        handleDelete(state._id);
        return { ...initialState };
      default:
        return {
          ...state,
          [field]: value,
        };
    }
  };

  const [state, dispatch] = useReducer(reducer, initialState);
  const { _id, description, amount, taxes, date } = state;

  const onChange = (e) => {
    dispatch({ field: e.target.name, value: e.target.value });
  };

  if (!state.editMode) {
    return (
      <div className="row" key={_id}>
        <div className="col-2">{description}</div>
        <div className="col-2">{amount}</div>
        <div className="col-2">{taxes}</div>
        <div className="col-3">{date}</div>
        <div className="col-sm-auto list-actions-wrapper">
          <Button
            variant="warning"
            onClick={() => dispatch({ field: "START_EDIT" })}
          >
            Edit
          </Button>
          <Button
            variant="danger"
            onClick={() => dispatch({ field: "DELETE" })}
          >
            Delete
          </Button>
        </div>
      </div>
    );
  } else {
    return (
      <div className="row" key={_id}>
        <div className="col-2">
          <input
            type="text"
            className="form-control"
            name="description"
            placeholder="Description"
            value={description}
            onChange={(e) => onChange(e)}
          />
        </div>
        <div className="col-2">
          <input
            type="number"
            className="form-control"
            name="amount"
            placeholder="0"
            value={amount}
            onChange={(e) => onChange(e)}
          />
        </div>
        <div className="col-2">{taxes}</div>
        <div className="col-3">
          <input
            type="date"
            className="form-control"
            name="date"
            placeholder="0"
            value={date}
            onChange={(e) => onChange(e)}
          />
        </div>
        <div className="col-sm-auto list-actions-wrapper">
          <Button
            variant="danger"
            onClick={() => dispatch({ field: "CANCEL" })}
          >
            Cancel
          </Button>
          <Button variant="success" onClick={() => dispatch({ field: "SAVE" })}>
            Save
          </Button>
        </div>
      </div>
    );
  }
};

export default Row;
