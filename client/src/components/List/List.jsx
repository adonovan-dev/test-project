import React from "react";
import Row from "./Row";
const List = ({ expenses }) => {
  if (!expenses) return null;
  // Sends PUT request to edit an expense
  const handleEdit = ({ _id, ...expense }) => {
    fetch(`/edit/${_id}`, {
      method: "PUT",
      body: JSON.stringify({
        expense,
      }),
      headers: { "Content-Type": "application/json" },
    }).then((response) => {
      if (response.status !== 200) {
        alert("Error");
      }
      // Dirty hack since Redux store is not implemented
      window.location.reload();
    });
  };

  // Sends DELETE request to delete an expense
  const handleDelete = (_id) => {
    fetch(`/delete/${_id}`, {
      method: "DELETE",
      body: JSON.stringify({
        _id,
      }),
      headers: { "Content-Type": "application/json" },
    }).then((response) => {
      if (response.status !== 200) {
        alert("Error");
      }
      // Dirty hack since Redux store is not implemented
      window.location.reload();
    });
  };

  return (
    <div className="ExpensesList">
      <div className="row">
        <div className="col-2">Description</div>
        <div className="col-2">Amount</div>
        <div className="col-2">Taxes (15%)</div>
        <div className="col-3">Date</div>
        <div className="col-sm-auto"></div>
      </div>

      {expenses.map((expense) => (
        <Row
          expense={expense}
          handleSave={handleEdit}
          handleDelete={handleDelete}
        />
      ))}
    </div>
  );
};

export default List;
