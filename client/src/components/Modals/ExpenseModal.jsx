import React, { useEffect, useReducer } from "react";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";

const ExpenseModal = ({ showModal, handleClose, onSave, expense }) => {
  const initialState = (() => {
    // If we recieve an expense we are editing. Set current data to initial state
    if (expense) {
      return expense;
    }
    return {
      description: "",
      amount: 0,
      taxes: 0,
      date: new Date(),
    };
  })();

  const reducer = (state, { field, value }) => {
    switch (field) {
      case "amount":
        console.log(value);
        // Set tax value on amount change
        return {
          ...state,
          taxes: (Math.ceil(parseFloat(value) * 0.15 * 20) / 20).toFixed(2),
          [field]: value.replace(/^0+/, ""),
        };
      case "reset_form":
        return { ...initialState };
      default:
        return {
          ...state,
          [field]: value,
        };
    }
  };

  const [state, dispatch] = useReducer(reducer, initialState);

  // Reset form data on open
  useEffect(() => {
    if (showModal) {
      dispatch({ field: "reset_form" });
    }
  }, [showModal]);

  const { description, amount, taxes, date } = state;

  const onChange = (e) => {
    dispatch({ field: e.target.name, value: e.target.value });
  };

  const handleSave = () => {
    onSave({ description, amount, taxes, date });
  };

  return (
    <Modal
      show={showModal}
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
    >
      <Modal.Header closeButton>
        <Modal.Title>Modal title</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="form-group">
          <label>Description</label>
          <input
            type="text"
            className="form-control"
            name="description"
            placeholder="Description"
            value={description}
            onChange={(e) => onChange(e)}
          />
        </div>
        <div className="form-group">
          <label>Amount</label>
          <input
            type="number"
            className="form-control"
            name="amount"
            placeholder="0"
            value={amount}
            onChange={(e) => onChange(e)}
          />
        </div>
        <div className="form-group">
          <label>Taxes (15%)</label>
          <input
            type="number"
            className="form-control"
            name="taxes"
            placeholder="0"
            disabled
            value={taxes}
            onChange={(e) => onChange(e)}
          />
        </div>
        <div className="form-group">
          <label>Date</label>
          <input
            type="date"
            className="form-control"
            name="date"
            placeholder="0"
            value={date}
            onChange={(e) => onChange(e)}
          />
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleClose}>
          Cancel
        </Button>
        <Button variant="primary" onClick={() => handleSave()}>
          Save
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default ExpenseModal;
