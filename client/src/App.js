import React, { Component } from "react";
import CreateFormModal from "./components/CreateFormModal";
import List from "./components/List/List";
import Button from "react-bootstrap/Button";

class App extends Component {
  state = {
    expenses: [],
    showCreateModal: false,
  };

  componentDidMount() {
    // Call our fetch function below once the component mounts
    this.callBackendAPI()
      .then(({ expenses }) => this.setState({ expenses }))
      .catch((err) => console.log(err));
  }
  // Fetch current expenses list
  callBackendAPI = async () => {
    const response = await fetch("/list");
    const body = await response.json();

    if (response.status !== 200) {
      throw Error(body.message);
    }
    return body;
  };

  closeModal = () => {
    this.setState({ showCreateModal: false });
  };

  getSubTotal = () => {
    return this.state.expenses.reduce((acc, { amount }) => {
      return acc + parseFloat(amount);
    }, 0);
  };
  getTotal = () => {
    return this.state.expenses.reduce((acc, { amount, taxes }) => {
      return acc + parseFloat(amount) + parseFloat(taxes);
    }, 0);
  };

  render() {
    return (
      <div className="App">
        <div className="container">
          <div className="row">
            <div className="col-lg">
              <h1>Expense Tracker</h1>
            </div>
          </div>
          <div className="row">
            <div className="col-sm">
              The sub-total of expenses is {this.getSubTotal()}$
            </div>
            <div className="col-sm"></div>
            <div className="col-sm">
              <Button
                variant="success"
                onClick={() => this.setState({ showCreateModal: true })}
              >
                Add new expense
              </Button>
            </div>
          </div>
          <div className="row">
            <div className="col-lg">
              The total with taxes is {this.getTotal()}$
            </div>
          </div>
          <div className="row">
            <div className="col-lg">
              <List expenses={this.state.expenses} />
            </div>
          </div>
          <CreateFormModal
            showModal={this.state.showCreateModal}
            closeModal={this.closeModal}
          />
        </div>
      </div>
    );
  }
}

export default App;
